//Text_Anim_00
//2023

//Anim-01
var i = 0;
var txt_01 = 'hiXOeb2X1wMZ2xromdcd 9uEiYWrk8NMYgbLFi0JF twWyK7W2ocEunRxlmafq uxjA6LyA03TJOMrZ1osy T2171R1MhpjxGpnSVZBp BrGGSoK1kn7qcWQQgyoz qHd6QlatBrA7CAMj2fTs fswRRUW2tGcc64rl319n';
var speed_01 = 50;

function type_writer_01() {
    if (i < txt_01.length) {
        document.getElementById("txt_render_01").innerHTML += txt_01.charAt(i);
        i++;
        setTimeout(type_writer_01, speed_01);
    }
}

//Anim-02
var j = 0;
var txt_02 = 'qRsy7Yn70VT8lh2tVQb5 ZZcpmkYLHPJ1iIuITmNV n913lr46ccGt2WURRwsf sTf2jMAC7ArBtalQ6dHq zoygQQWcq7nk1KoSGGrB pBZVSnpGxjphM1R1712T yso1ZrMOJT30AyL6Ajxu qfamlxRnuEco2W7KyWwt';
var speed_02 = 60;

function type_writer_02() {
    if (j < txt_02.length) {
        document.getElementById("txt_render_02").innerHTML += txt_02.charAt(j);
        j++;
        setTimeout(type_writer_02, speed_02);
    }
}

//Anim-03
var k = 0;
var txt_03 = '5bQVt2hl8TV07nY7ysRq VNmTIuIi1JPHLYkmpcZZ fswRRUW2tGcc64rl319n qHd6QlatBrA7CAMj2fTs BrGGSoK1kn7qcWQQgyoz T2171R1MhpjxGpnSVZBp uxjA6LyA03TJOMrZ1osy fswRRUW2tGcc64rl319n';
var speed_03 = 40;

function type_writer_03() {
    if (k < txt_03.length) {
        document.getElementById("txt_render_03").innerHTML += txt_03.charAt(k);
        k++;
        setTimeout(type_writer_03, speed_03);
    }
}

//Anim-04
const date_time = new Date();
let date_conv = date_time.toString();
var l = 0;
var txt_04 = date_conv + ' ' + 'Created_by_#7779d1_2022-2023';
var speed_04 = 100;

function type_writer_04() {
    if (l < txt_04.length) {
        document.getElementById("txt_render_04").innerHTML += txt_04.charAt(l);
        l++;
        setTimeout(type_writer_04, speed_04);
    }
}

//Start
window.onload = function () {
    type_writer_01();
    type_writer_02();
    type_writer_03();
    type_writer_04();
}


